const express = require('express');
const app = express.Router();
let access = require('../lib/jwt');

/*Token Generation for each user*/
var token = require('../components/v1/token/bin/controller');
app.use('/api/v1', token);
/*End of Token Generation for each user*/

/* user*/
var user = require('../components/v1/user/bin/controller');
app.use('/api/v1/user', access.validateJwt, user);
/*End of  user*/

module.exports = app;
