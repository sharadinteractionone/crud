require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
global._LOGGER = require('../crud/lib/logger');
global._UTIL = require('../crud/lib/util');
const app = express();

/**logging info */
const loggerMiddleware = (req, res, next) => {
	console.log(req.method + ' ' + req.path);
	next();
};

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(loggerMiddleware);

/**Setting Mongo Connection */
mongoose.connect(process.env.MONGO_DB, {
	useUnifiedTopology: true,
	useCreateIndex: true,
	useNewUrlParser: true,
});

global._JWT_SECRET = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJpYXQiOj';
global._JWT_ISSUER = 'Battle Node API';
global._JWT_HEADER = 'X-ACCESS-TOEKN';
global._CRYPT_KEY = 'E1MDc2MzQ5ODQsImp0aSI6IlwvUUlEVk0rVTJ1Zj';

/**Setting Router */
var router = require('./routes/router');
app.use(router);

module.exports = app;
