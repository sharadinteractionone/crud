const express = require('express');
const router = express.Router();
var userModel = require('../lib/model');

router.post('/create', async (req, res) => {
	try {
		if (!req.body.name || !req.body.email || !req.body.phone) return res.json({ success: false, message: 'BAD_REQUEST' });
		let userData = await userModel.createUser(req.body);
		return res.send(_UTIL.successOk({ user: userData }));
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
		return res.send(_UTIL.parseError(e.toString()));
	}
});

router.post('/create/many', async (req, res) => {
	try {
		if (!req.body.users) return res.json({ success: false, message: 'BAD_REQUEST' });
		let userData = await userModel.createManyUsers(req.body.users);
		return res.send(_UTIL.successOk({ user: userData }));
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
		return res.send(_UTIL.parseError(e.toString()));
	}
});

router.get('/', async (req, res) => {
	try {
		if (!req.body.userId) return res.json({ success: false, message: 'BAD_REQUEST' });
		let userData = await userModel.getUserByUserId(req.body.userId);
		return res.send(_UTIL.successOk({ user: userData }));
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
		return res.send(_UTIL.parseError(e.toString()));
	}
});

router.put('/update', async (req, res) => {
	try {
		if (!req.body.userId) return res.json({ success: false, message: 'BAD_REQUEST' });
		let userData = await userModel.updateuserByUserId(req.body);
		return res.send(_UTIL.successOk({ user: userData }));
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
		return res.send(_UTIL.parseError(e.toString()));
	}
});

router.delete('/', async (req, res) => {
	try {
		if (!req.body.userId) return res.json({ success: false, message: 'BAD_REQUEST' });
		let userData = await userModel.deleteUserByUserId(req.body.userId);
		return res.send(_UTIL.successOk({ user: userData }));
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
		return res.send(_UTIL.parseError(e.toString()));
	}
});

router.delete('/many', async (req, res) => {
	try {
		if (!req.body.users) return res.json({ success: false, message: 'BAD_REQUEST' });
		let userData = await userModel.deleteMultipleUsers(req.body);
		return res.send(_UTIL.successOk({ user: userData }));
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
		return res.send(_UTIL.parseError(e.toString()));
	}
});

module.exports = router;
