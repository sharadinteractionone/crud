const mongoose = require('mongoose');

let userSchema = new mongoose.Schema(
	{
		name: { type: String },
		email: { type: String },
		phone: { type: String },
		enabled: { type: Number, default: 1 },
	},
	{ timestamps: true }
);
const users = mongoose.model('users', userSchema);

module.exports = { users };
