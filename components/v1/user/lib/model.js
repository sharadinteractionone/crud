let userSchema = require('./schema');
let self = (module.exports = {});

self.createUser = function (reqBody) {
	return new Promise(async (resolve, reject) => {
		try {
			let userObj = await userSchema.users.findOne({ email: reqBody.email });
			if (userObj) {
				return resolve('User Is Already Created, Please Login !!');
			} else {
				let userObj = {
					name: reqBody.name,
					email: reqBody.email,
					phone: reqBody.phone,
				};
				let createdUser = await userSchema.users.create(userObj);
				return resolve(createdUser);
			}
		} catch (e) {
			return reject(e.toString);
		}
	});
};

self.getUserByUserId = function (userId) {
	return new Promise(async (resolve, reject) => {
		try {
			let user = await userSchema.users.findOne({ _id: userId });
			if (user) return resolve(user);
			return resolve('User Not Created Yet, Please SignUp !!');
		} catch (e) {
			return reject(e.toString);
		}
	});
};

self.updateuserByUserId = function (reqBody) {
	return new Promise(async (resolve, reject) => {
		try {
			let userObj = await userSchema.users.findOne({ _id: reqBody.userId });
			if (userObj) {
				let updateQuery = {};
				if (reqBody.name) updateQuery.name = reqBody.name;
				if (reqBody.email) updateQuery.email = reqBody.email;
				if (reqBody.phone) updateQuery.phone = reqBody.phone;
				console.log(updateQuery);
				await userSchema.users.updateOne({ _id: reqBody.userId }, { $set: updateQuery });
				let user = await userSchema.users.findOne({ _id: reqBody.userId });
				return resolve(user);
			} else {
				return resolve('User Not Created Yet, Please SignUp !!');
			}
		} catch (e) {
			return reject(e.toString);
		}
	});
};

self.deleteUserByUserId = function (userId) {
	return new Promise(async (resolve, reject) => {
		let userObj = await userSchema.users.findOne({ _id: userId });
		if (userObj) {
			let userDeleteStatus = await userSchema.users.deleteOne({ _id: userId });
			return resolve(userDeleteStatus);
		} else {
			return resolve('User Not Created Yet, Please SignUp !!');
		}
	});
};

self.createManyUsers = function (users) {
	return new Promise((resolve, reject) => {
		if (users.length == 0) return reject('No Users Found');
		let index = 0;
		let createUsersArray = [];
		let totatCount = users.length;
		async function next(index) {
			console.log(index, totatCount);
			if (index === totatCount) {
				let createdUsers = await userSchema.users.insertMany(createUsersArray);
				return resolve(createdUsers);
			} else {
				setTimeout(() => {
					process.nextTick(iterate);
				}, 100);
			}
		}

		function iterate() {
			let user = users[index];
			let userObj = {
				name: user.name,
				email: user.email,
				phone: user.phone,
			};
			console.log(userObj);
			createUsersArray.push(userObj);
			next(++index);
		}

		iterate();
	});
};

self.deleteMultipleUsers = function (reqBody) {
	return new Promise(async (resolve, reject) => {
		if (reqBody.users.length == 0) return reject('No Users Found');
		let index = 0;
		let totatCount = reqBody.users.length;
		let deletedCount = 0;
		let notValidUsersCount = 0;
		function next(index) {
			console.log(index, totatCount);
			if (index === totatCount) {
				return resolve({ total: totatCount, deletedCount: deletedCount, notValidUsersCount: notValidUsersCount });
			} else {
				setTimeout(() => {
					process.nextTick(iterate);
				}, 100);
			}
		}

		async function iterate() {
			let userId = reqBody.users[index];
			let userData = await userSchema.users.findOne({ _id: userId });
			if (userData) {
				await userSchema.users.deleteOne({ _id: userId });
				deletedCount++;
				next(++index);
			} else {
				notValidUsersCount++;
				next(++index);
			}
		}

		iterate();
	});
};
