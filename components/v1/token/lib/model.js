const moment = require('moment');
const jwt = require('jwt-simple');

let self = (module.exports = {});
const jwtAttributes = {
	SECRET: _JWT_SECRET,
	ISSUER: _JWT_ISSUER,
	HEADER: _JWT_HEADER,
};

self.createToken = function (authObj) {
	return new Promise((resolve, reject) => {
		const { EXPIRY, ISSUER, SECRET } = jwtAttributes;
		let token;
		let payload = {
			iss: ISSUER,
			name: authObj.name,
			userId: authObj.password,
		};
		try {
			token = jwt.encode(payload, SECRET);
		} catch (e) {
			console.log(e.toString());
			return reject('Unauthorized Access: Access Token unable to create');
		}
		return resolve({ token: token });
	});
};
