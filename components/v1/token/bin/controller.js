const express = require('express');
const router = express.Router();
var jwtModel = require('../lib/model');

router.post('/authorize', async (req, res) => {
	try {
		if (!req.body.username || !req.body.password) return res.json({ success: false, message: 'BAD_REQUEST' });
		let obj = {
			name: req.body.username,
			password: req.body.password,
		};
		let resp = await jwtModel.createToken(obj);
		res.send(_UTIL.successOk({ message: 'User access has been granted', access_token: resp.token }));
	} catch (e) {
		console.log(e.toString());
		_LOGGER.info(e.toString());
		return res.send(_UTIL.parseError(e.toString()));
	}
});

module.exports = router;
