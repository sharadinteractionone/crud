var app = require('../index');
var http = require('http');
const CHALK = require('chalk');
var port = normalizePort(process.env.PORT || '3132');

app.set('port', port);
var server = http.createServer(app);

console.log(CHALK.blueBright('*******************************'));
console.log(CHALK.blueBright('*          EMBASSY - API      *'));
console.log(CHALK.blueBright('*******************************'));
console.log('Ready & Listening on port ' + port);
server.timeout = 15000;
server.listen(port);
server.on('timeout', function (e) {
	// console.log('********************* REQUESTED OPERATION TIMEOUT ***************************')
});
server.on('error', onError);
server.on('listening', onListening);

process.on('UncaughtException', function (e) {
	console.log(CHALK.red('UncaughtException >> ', e));
});

function normalizePort(val) {
	var port = parseInt(val, 10);
	if (isNaN(port)) {
		return val;
	}
	if (port >= 0) {
		return port;
	}
	return false;
}

function onError(error) {
	if (error.syscall !== 'listen') {
		throw error;
	}
	var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;
	// handle specific listen errors with friendly messages
	switch (error.code) {
		case 'EACCES':
			console.error(bind + ' requires elevated privileges');
			process.exit(1);
			break;
		case 'EADDRINUSE':
			console.error(bind + ' is already in use');
			process.exit(1);
			break;
		default:
			throw error;
	}
}

function onListening() {
	var addr = server.address();
	var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
}
