const jwt = require('./jwt');

exports.validateJwt = async (req, res, next) => {
	try {
		if (req.headers && req.headers['authorization']) {
			let accessToken = req.headers['authorization'];
			console.log('>>>>>>>>>>>', accessToken);
			let token = await jwt.validateToken(accessToken);
			req.user = token;
			return next();
		} else {
			return res.send(_UTIL.parseERR('Unauthorized Access: Access Token not found / expired'));
		}
	} catch (e) {
		return res.send(_UTIL.parseERR('Unauthorized Access: Access Token not found / expired'));
	}
};
