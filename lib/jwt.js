const { compareSync } = require('bcryptjs');
const jwt = require('jwt-simple');

const jwtAttributes = {
	SECRET: _JWT_SECRET,
	ISSUER: _JWT_ISSUER,
	HEADER: _JWT_HEADER,
};

module.exports.validateToken = function (token) {
	return new Promise((resolve, reject) => {
		const { HEADER, SECRET } = jwtAttributes;
		let decodedToken;
		try {
			decodedToken = jwt.decode(token, SECRET);
		} catch (e) {
			return reject('Unauthorized Access: Access Token is Invalid');
		}
		return resolve(decodedToken);
	});
};

module.exports.validateJwt = async (req, res, next) => {
	try {
		if (req.headers && req.headers['authorization']) {
			let accessToken = req.headers['authorization'];
			let token = await this.validateToken(accessToken);
			req.user = token;
			return next();
		} else {
			return res.send(_UTIL.parseError('Unauthorized Access: Access Token not found / expired'));
		}
	} catch (e) {
		console.log(e.toString());
		return res.send(_UTIL.parseError('Unauthorized Access: Access Token not found / expired //'));
	}
};
