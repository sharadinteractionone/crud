let _this = (module.exports = {});

_this.parseOk = function (e, v) {
	return {
		value: { ok: e ? false : true, code: 0, reason: e ? e : undefined },
		data: v,
		timestamp: new Date().getTime(),
	};
};

_this.parseError = function (v) {
	return {
		success: false,
		message: v,
	};
};

_this.successOk = function (v, e) {
	return {
		success: true,
		data: e ? undefined : v,
		message: e ? v : undefined,
	};
};
